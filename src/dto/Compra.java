package dto;

public class Compra {
	
	private int numerodecompra;
	private Cliente cliente;
	private double monto;
	private Bicicleta bici;
	private Date fechadecompra;
	private String mediodepago;
	public Compra() {
		super();
	}
	public Compra(String numerodecompra, Cliente cliente, double monto,
			Bicicleta bici, Date fechadecompra, String mediodepago) {
		super();
		this.numerodecompra = numerodecompra;
		this.cliente = cliente;
		this.monto = monto;
		this.bici = bici;
		this.fechadecompra = fechadecompra;
		this.mediodepago = mediodepago;
	}
	public int getNumerodecompra() {
		return numerodecompra;
	}
	public void setNumerodecompra(int compran) {
		this.numerodecompra = compran;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public Bicicleta getBici() {
		return bici;
	}
	public void setBici(Bicicleta bici) {
		this.bici = bici;
	}
	public Date getFechadecompra() {
		return fechadecompra;
	}
	public void setFechadecompra(Date fechadecompra) {
		this.fechadecompra = fechadecompra;
	}
	public String getMediodepago() {
		return mediodepago;
	}
	public void setMediodepago(String mediodepago) {
		this.mediodepago = mediodepago;
	}
	
	
}
