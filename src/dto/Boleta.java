package dto;

public class Boleta {
private int numeroFactura;
private Cliente cliente;
private Pago pago;
private double descuento;
private Producto bicicleta;
private Date fecha;
public int getNumeroFactura() {
	return numeroFactura;
}
public void setNumeroFactura(int numeroFactura) {
	this.numeroFactura = numeroFactura;
}
public Cliente getCliente() {
	return cliente;
}
public void setCliente(Cliente cliente) {
	this.cliente = cliente;
}
public Pago getPago() {
	return pago;
}
public void setPago(Pago pago) {
	this.pago = pago;
}
public double getDescuento() {
	return descuento;
}
public void setDescuento(double descuento) {
	this.descuento = descuento;
}
public Producto getBicicleta() {
	return bicicleta;
}
public void setBicicleta(Producto bicicleta) {
	this.bicicleta = bicicleta;
}
public Date getFecha() {
	return fecha;
}
public void setFecha(Date fecha) {
	this.fecha = fecha;
}


}
