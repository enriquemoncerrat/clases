package dto;

public class Bicicleta {
	private String color;
	private String modelo;
	private int ruedadeInercia;
	private String estructura;
	private String asiento;
	private String manubrio;
	private String pedales;
	
	
	
	
	public Bicicleta() {
		super();
	}
	
	public Bicicleta(String color, String modelo, int ruedadeInercia, String estructura, String asiento,
			String manubrio, String pedales) {
		super();
		this.color = color;
		this.modelo = modelo;
		this.ruedadeInercia = ruedadeInercia;
		this.estructura = estructura;
		this.asiento = asiento;
		this.manubrio = manubrio;
		this.pedales = pedales;
	}
	public void setEstructura(String estructura) {
		this.estructura = estructura;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public int getRuedadeInercia() {
		return ruedadeInercia;
	}
	public void setRuedadeInercia(int ruedadeInercia) {
		this.ruedadeInercia = ruedadeInercia;
	}
	public String getEstructura() {
		return estructura;
	}
	public void SetEstructura(String estructura) {
		this.estructura = estructura;
	}
	public String getAsiento() {
		return asiento;
	}
	public void setAsiento(String asiento) {
		this.asiento = asiento;
	}
	public String getManubrio() {
		return manubrio;
	}
	public void setManubrio(String manubrio) {
		this.manubrio = manubrio;
	}
	public String getPedales() {
		return pedales;
	}
	public void setPedales(String pedales) {
		this.pedales = pedales;
	}
	

}
