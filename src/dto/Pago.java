package dto;

public class Pago {
	private double monto;
	private Cliente cliente;
	private Bicicleta bicicleta;
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Bicicleta getBicicleta() {
		return bicicleta;
	}
	public void setBicicleta(Bicicleta bicicleta) {
		this.bicicleta = bicicleta;
	}
	public Pago(double monto, Cliente cliente, Bicicleta bicicleta) {
		super();
		this.monto = monto;
		this.cliente = cliente;
		this.bicicleta = bicicleta;
	}
	public Pago() {
		super();
	}
	
	

}
